"""def digit(number, order)
    return number % (10 ** order) / (10 ** (order - 1))


def is_magic(number):
    half1sum = digit(number, 6) + digit(number, 5) + digit(number, 4)
    half2sum = digit(number, 3) + digit(number, 2) + digit(number, 1)
    return half1sum  == half2sum

number1 = int(input("enter first number: "))
number2 = int(input("enter second number: "))

magic_count = 0
for x in range(number1, number2 + 1):
    if is_magic(x):
        print("{} is magic number!".format(number))
        magic_count += 1
        
print("{} magic numbers in the range from {} to {}".format(magic_count, number1, number2))"""

num=int(input("Enter number:"))
sum=0
while num>0:
    rm=num%10
    sum +=rm
    num=num//10
print("sum of digits:",sum)
 
if sum==1:
    print("Number is magical")
else:
    print("Number is not magical")