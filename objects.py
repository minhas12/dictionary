class Student:
    branch="CSE"
    college = "Abcd Engineering College"

    def information(self,name,rno,email):
        print("Branch:",self.branch)
        print("Name: {} \n Roll No: {}\n Email: {}".format(name,rno,email))
        print("*****************\n")

    def ab(self,v):
        rs=lambda x:x**2
        return rs(v)  

    def sq(self,val):
        def square(v):
            return v**2
            return square(val)  

ob=Student()
ob1=Student()
ob2=Student()
print(ob.college,"({})".format(ob.branch))

ob.information("Aman",1,"aman@gmail.com")
ob.information("Guido",2,"rossum@gmail.com")
ob.information("Harry Potter",2,"potter@gmail.com")
print(ob.ab(10))
print(ob.sq(100))
