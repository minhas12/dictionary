ab={
   "name" : "Harry Potter",
   "Age":17,
   "friends":["Hermione","Ron","Ginnie","Neville"],
   "Hobbies":("Magic","Parseltonge"),
   "Platform No.":9.75,
   "Muggle":False,
   }
print(ab["name"],type(ab["name"]))
print(ab["friends"][1])
print(ab["Muggle"])

#Looping through a dict

for n,v in enumerate(ab["friends"],1):
    print(n,v)

    for i in ab:
        print(i,'=',ab[i])

        #print(ab.values())
        #print(ab.keys())
        #print(ab.items())

for i in ab.items():
    print(i)

for k,v in ab.items():
    print("Key: {} Value:{}".format(k,v))
