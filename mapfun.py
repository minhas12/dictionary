ls=["aMan","heLLo","hII","peTer","harry pOTtEr","lOrem"]
marks=[100,90,7,33,20,3,45,79]

def check(st):
    return st.title()

def perc(n):
    p=(n/120)*100
    return round(p,3)
    
result=list(map(check,ls))
print(result)

print(marks)
percentage=list(map(perc,marks))
print(percentage)

#by_lambda:

print(list(map(lambda a:a.title(),ls)))