#creating class
class Employee:
    #data member
    branch="SE"
    company="SachTech Solution Pvt. Ltd."

    #member function
    def info(anb):
        print("Member Function")

#CREATING OBJECT OF A CLASS
ob = Employee()

#accessing data member
print(ob.branch)

#accessing member function
ob.info()
