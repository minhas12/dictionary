#positional arguments
#keyword arguments
#arbitary arguments
#default arguments

def employee(name="Mr.Abcd",Salary=0):
    print("Name:{} Salary:Rs:{}/-".format(name,salary))

    #Positional Arguments
    employee("Peter",2000)
    employee(150000,"Shinchan")

    #Keyword Arguments
    employee(salary=15000,name="Shinchan")
    employee("Peter",name="parker") #incorrect

    employee("Hary Potter")
    employee("Harry Potter",10000)